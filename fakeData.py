from faker import Faker
import random
import requests
import time

fake = Faker('zh_TW')

class Contact:
    def __init__(self, name :str, phoneNumber: str, address: str, age: int, email: str, gender: str):
        self.name = name
        self.phoneNumber = phoneNumber
        self.address = address
        self.age = age
        self.email = email
        self.gender = gender

dataNum = 20000

startTime = time.time()
for _ in range(dataNum):
    phoneNumber: str = fake.phone_number()
    address: str = fake.address()
    age: int = random.randint(12, 90)
    email: str = fake.ascii_free_email()
    gender: str = 'male' if random.randint(0,3) > 1  else 'female'
    _firstName = fake.first_name_male() if gender is 'male' else fake.first_name_female()
    name:str = fake.last_name() + _firstName

    _contact = Contact(name, phoneNumber, address, age, email, gender)
    requests.post('http://localhost:3000/contact', data = _contact.__dict__)
    #print(_contact.__dict__)
print("Total cost time: " + str(time.time() - startTime))