export class CreateContactDto{
    name: string;
    phoneNumber: string;
    address: string;
    age: number;
    email: string;
    gender: string;
}