import { Injectable } from '@nestjs/common';
import { Contact, ContactDocument } from 'src/schemas/contact.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ContactStore } from 'src/stores/contact.store';

@Injectable()
export class StoreInit {
    constructor(@InjectModel(Contact.name) private contactModel: Model<ContactDocument>){
        this.contactModel.find().
            exec((err, data: Contact[])=>{
                if(!err)
                {
                    data.forEach(contact => ContactStore.state.set(contact._id, contact));
                }
            });
    }
}
