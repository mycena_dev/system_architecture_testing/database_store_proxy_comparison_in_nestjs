import { Contact } from 'src/schemas/contact.schema'

export class ContactStore{
    static state: Map<string, Contact> = new Map<string, Contact>();

    static upsert(contact: Contact)
    {
        ContactStore.state.set(contact._id, contact);
    }

    static getAll(): Contact[]
    {
        var contacctArray = [...ContactStore.state.values()];
        return Array.from(ContactStore.state.values());
    }

    static getAllId(): string[]
    {
        return Array.from(ContactStore.state.keys());
    }

    static removeById(id: string): string 
    {
        if (ContactStore.state.has(id))
        {
            ContactStore.state.delete(id);
        }
        return id;
    }

    static getById(id: string) : Contact
    {
        if (ContactStore.state.has(id))
            return ContactStore.state.get(id);
        return null;
    }

    static filterLimitAge(limitAge: number) : Contact[]
    {
        const result : Contact[] = [];
        ContactStore.state.forEach((contact: Contact, key: string) => 
        {
            if(contact.age>=limitAge)
            {
                result.push(contact)
            }
        });
        return result;
    }
}