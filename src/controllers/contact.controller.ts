import { Controller, Post, Get, Body, Param, Delete, Query} from '@nestjs/common';
import { CreateContactDto } from 'src/DTO/create-contact.dto';
import { ContactService } from 'src/services/contact.service';

@Controller('contact')
export class ContactController{
    constructor(private contactService: ContactService){}

    @Post()
    async create(@Body() createContactDto: CreateContactDto){
        this.contactService.create(createContactDto);
    }

    @Get('allFromStore')
    async findAddFromStore()
    {
        console.log("Trigger allFromStore", Date.now());
        return this.contactService.findAllFromStore();
    }

    @Get('proxyServer/allFromStore/')
    async findAddFromStoreOverProxyServer()
    {
        console.log("Trigger proxyServer/allFromStore", Date.now());
        return this.contactService.findAllFromStore();
    }

    @Get('allFromDatabase')
    async findAddFromDatabase()
    {
        console.log("Trigger allFromDatabase", Date.now());
        return this.contactService.findAllFromDataBase();
    }

    @Get('allIdFromStore')
    async findAllIdFromSotre(){
        console.log("Trigger allIdFromStore", Date.now());
        return this.contactService.findAllIdFromStore();
    }

    @Get('proxyServer/allIdFromStore')
    async findAllIdFromSotreOverProxyServer(){
        console.log("Trigger proxyServer/allIdFromStore'", Date.now());
        return this.contactService.findAllIdFromStore();
    }

    @Get('allIdFromDatabase')
    async findAllIdFromDatabase(){
        console.log("Trigger allIdFromDatabase", Date.now());
        return this.contactService.findAllIdFromDataBase();
    }

    @Get('/fromStore/:id')
    async findOneFromStore(@Param('id') id: string) {
        console.log(`Trigger fromStore [${id}]`, Date.now());
        return this.contactService.findOneByIdFromStore(id);
    }

    @Get('proxyServer/fromStore/:id')
    async findOneFromStoreOverProxyServer(@Param('id') id: string) {
        console.log(`Trigger proxyServer/fromStore [${id}]`, Date.now());
        return this.contactService.findOneByIdFromStore(id);
    }

    @Get('/fromDatabase/:id')
    async findOneFromDatabase(@Param('id') id: string) {
        console.log(`Trigger fromDatabase [${id}]`, Date.now());
        return this.contactService.findOneByIdFromDataBase(id);
    }

    @Get('/fromStoreWithAgeCondition')
    async fromStoreWithAgeCondition(@Query('limitAge') limitAge) {
        console.log(`Trigger fromStoreWithAgeCondition api with limitAge ${limitAge}`, Date.now());
        return this.contactService.filterLimitAgeFromStore(limitAge);
    }

    @Get('proxyServer/fromStoreWithAgeCondition')
    async fromStoreWithAgeConditionOverProxyServer(@Query('limitAge') limitAge) {
        console.log(`Trigger proxyServer/fromStoreWithAgeCondition api with limitAge ${limitAge}`, Date.now());
        return this.contactService.filterLimitAgeFromStore(limitAge);
    }

    @Get('/fromDatabaseWithAgeCondition')
    async fromDatabaseWithAgeCondition(@Query('limitAge') limitAge) {
        console.log(`Trigger fromDatabaseWithAgeCondition api with limitAge ${limitAge}`, Date.now());
        return this.contactService.filterLimitAgeFromDataBase(limitAge);
    }

    @Delete(':id')
    async remove(@Param('id') id: string) {
        console.log("Trigger delete api", Date.now());
        this.contactService.delete(id);
    }
}