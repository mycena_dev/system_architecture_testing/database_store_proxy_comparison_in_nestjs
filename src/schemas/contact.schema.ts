import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ContactDocument = Contact & Document;

@Schema()
export class Contact {

    _id: string

    @Prop()
    name: string;

    @Prop()
    phoneNumber: string;

    @Prop()
    address: string;

    @Prop()
    age: number;

    @Prop()
    email: string;

    @Prop()
    gender: string;
}

export const ContactSchema = SchemaFactory.createForClass(Contact);