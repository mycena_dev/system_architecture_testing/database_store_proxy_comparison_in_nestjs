import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ContactController } from './controllers/contact.controller';
import { ContactService } from './services/contact.service';
import { Contact, ContactSchema } from './schemas/contact.schema';
import { StoreInit } from './agents/store-init.agent'

@Module({
  imports: [MongooseModule.forRoot('mongodb://localhost/nest'),
  MongooseModule.forFeature([{ name: Contact.name, schema: ContactSchema }])],
  controllers: [AppController, ContactController],
  providers: [AppService, ContactService, StoreInit]
})
export class AppModule {}
