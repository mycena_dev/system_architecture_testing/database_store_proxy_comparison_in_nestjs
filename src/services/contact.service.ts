import { Injectable } from '@nestjs/common';
import { Contact, ContactDocument } from 'src/schemas/contact.schema';
import { CreateContactDto } from 'src/DTO/create-contact.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ContactStore } from 'src/stores/contact.store';

@Injectable()
export class ContactService {

    constructor(@InjectModel(Contact.name) private contactModel: Model<ContactDocument>){}
    
    async create(createContactDto: CreateContactDto): Promise<Contact> {
        return new Promise((resolve, reject) =>{
            const createdContact = new this.contactModel(createContactDto);
            createdContact.save((err, data: Contact) => {
                if(err){
                    const builtError = {
                        errorCode: 'ERR_MONGO_OPERATION_FAILED',
                        errorMessage: 'Cannot create Contact',
                        statusCode: 400,
                        source: 'Mongo DB',
                        errorMeta: err
                    };
                    return reject(builtError);
                }
                ContactStore.upsert(data);
                resolve(data);
            });
        });
    }

    async findAllIdFromStore():  Promise<string[]>{
        return new Promise((resolve, reject) => {
            try{
                resolve(ContactStore.getAllId());
            }
            catch(err)
            {
                const builtError = {
                    errorCode: 'ERR_STORE_OPERATION_FAILED',
                    errorMessage: 'Cannot find all id from store',
                    statusCode: 500,
                    source: 'STORE',
                    errorMeta: err
                };
                return reject(builtError);
            }
        });
    }

    async findAllIdFromDataBase(): Promise<string[]> {
        return new Promise((resolve, reject) => {
                this.contactModel.find().select('_id').
                exec((err, data: any[]) => {
                    if(err)
                    {
                        const builtError = {
                            errorCode: 'ERR_MONGO_OPERATION_FAILED',
                            errorMessage: 'Cannot find all ids from database',
                            statusCode: 500,
                            source: 'Mongo DB',
                            errorMeta: err
                        };
                        return reject(builtError);
                    }
                    resolve(data.map(item => item._id));
                }); 
            });
    }

    async findAllFromStore():  Promise<Contact[]>{
        return new Promise((resolve, reject) => {
            try{
                var contactList = ContactStore.getAll();
                resolve(contactList);
            }
            catch(err)
            {
                const builtError = {
                    errorCode: 'ERR_STORE_OPERATION_FAILED',
                    errorMessage: 'Cannot find all from store',
                    statusCode: 500,
                    source: 'STORE',
                    errorMeta: err
                };
                return reject(builtError);
            }
        });
    }

    async findAllFromDataBase(): Promise<Contact[]> {
        return new Promise((resolve, reject) => {
            this.contactModel.find().
            exec((err, data: Contact[])=>{
                if(err)
                {
                    const builtError = {
                        errorCode: 'ERR_MONGO_OPERATION_FAILED',
                        errorMessage: 'Cannot find all from database',
                        statusCode: 500,
                        source: 'Mongo DB',
                        errorMeta: err
                    };
                    return reject(builtError);
                }
                resolve(data);
            });
        });
    }

    async findOneByIdFromStore(id: string): Promise<Contact>{
        return new Promise((resolve, reject) => {
            try{
                resolve(ContactStore.getById(id));
            }
            catch(err)
            {
                const builtError = {
                    errorCode: 'ERR_STORE_OPERATION_FAILED',
                    errorMessage: 'Cannot find one by id from store',
                    statusCode: 500,
                    source: 'STORE',
                    errorMeta: err
                };
                return reject(builtError);
            }
        })
    }

    async findOneByIdFromDataBase(id: string): Promise<Contact>{
        return new Promise((resolve, reject) => {
            this.contactModel.findById(id).
            exec((err, data:Contact) => {
                if(err){
                    const builtError = {
                        errorCode: 'ERR_MONGO_OPERATION_FAILED',
                        errorMessage: 'Cannot find one by id from database',
                        statusCode: 500,
                        source: 'Mongo DB',
                        errorMeta: err
                    };
                    return reject(builtError);
                }
                resolve(data);
            })
        })
    }

    async filterLimitAgeFromStore(limitAge: number): Promise<Contact[]>{
        return new Promise((resolve, reject) => {
            try{
                resolve(ContactStore.filterLimitAge(limitAge));
            }
            catch(err)
            {
                const builtError = {
                    errorCode: 'ERR_STORE_OPERATION_FAILED',
                    errorMessage: 'Cannot find one by id from store',
                    statusCode: 500,
                    source: 'STORE',
                    errorMeta: err
                };
                return reject(builtError);
            }
        })
    }

    async filterLimitAgeFromDataBase(limitAge: number): Promise<Contact[]>{
        return new Promise((resolve, reject) => {
            this.contactModel.find({age:{$gte: limitAge}}).
            exec((err, data:Contact[]) => {
                if(err){
                    const builtError = {
                        errorCode: 'ERR_MONGO_OPERATION_FAILED',
                        errorMessage: 'Cannot find one by id from database',
                        statusCode: 500,
                        source: 'Mongo DB',
                        errorMeta: err
                    };
                    return reject(builtError);
                }
                resolve(data);
            })
        })
    }

    async delete(id: string): Promise<Contact>
    {
        return new Promise((resolve, reject) => {
            this.contactModel.findByIdAndDelete(id).
            exec((err, data: Contact) => {
                if(err){
                    const builtError = {
                        errorCode: 'ERR_MONGO_OPERATION_FAILED',
                        errorMessage: 'Cannot delete Contact',
                        statusCode: 500,
                        source: 'Mongo DB',
                        errorMeta: err
                    };
                    return reject(builtError);
                }
                ContactStore.removeById(id);
                resolve(data);
            });
        });
    };
}